const api = 'https://api.themoviedb.org/3/search/movie?api_key=c4ca21a45bf70168646932fc551d7d47&language=en-US&query=';
const img = 'https://image.tmdb.org/t/p/w500';
const toprated = 'https://api.themoviedb.org/3/movie/top_rated?api_key=c4ca21a45bf70168646932fc551d7d47&language=en-US&page='
const actorname = 'https://api.themoviedb.org/3/search/person?api_key=c4ca21a45bf70168646932fc551d7d47&language=en-US&query='
const detail = 'https://api.themoviedb.org/3/movie/'
const actordetail = 'https://api.themoviedb.org/3/person/'
const find = 'https://api.themoviedb.org/3/find/'


function searchMovies(e)
{
    searchMoviesByName(e);
    searchMoviesByActorName();
}

async function searchMoviesByName(e)
{
    e.preventDefault();
    const strSearch = $('form input').val();
 
    const req = `${api}${strSearch}`;
    loading();

    const response = await fetch(req);
    const myJson = await response.json();

    fillMovies(myJson, 'movie');

}

async function searchMoviesByActorName()
{
    const strSearch = $('form input').val();
    
    const i=1;
    const req = `${actorname}${strSearch}&page=${i}`;
    loading();

    const response = await fetch(req);
    const myJson = await response.json();

    fillMovies(myJson, 'actor');
}

async function topMovies()
{
    const i=1;
    const req = `${toprated}${i}`;

    const response = await fetch(req);
    const myJson = await response.json();

    fillMovies(myJson, 'movie');
}


// Hàm Điền thông tin phim theo searchby.
// Nếu searchby là 'movies' thì sẽ điền thông tin theo phim.
// Trường hợp khác sẽ điền thông tin theo diễn viên.
function fillMovies(text, searchby)
{   
    if (searchby == 'movie')
    {
        $('#main').empty();
        var i=0;
        while (text.results[i]) 
        {
            const m = text.results[i];
            $('#main').append(`
            <div class="col-md-4 col-md-offset-1">
                <div class="card" >
                <img onclick = "loadDetail(${m.id})" src="${img}${m.poster_path}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">${m.original_title}<br><br>Vote rate:${m.vote_average}</h5>
                        <p class="card-text">${m.overview}</p>
                    </div>
                </div>
            </div>
        `);
        i=i+1;
        }
    }
    else
    {
        var i=0;
        while (text.results[0].known_for[i]) 
        {
            const m = text.results[0].known_for[i];
            $('#main').append(`
            <div class="col-md-4 col-md-offset-1">
                <div class="card" >
                <img onclick ="loadDetail(${m.id})" src="${img}${m.poster_path}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">${m.original_title}<br><br>${m.vote_average}</h5>
                        <p class="card-text">${m.overview}</p>
                    </div>
                </div>
            </div>
            `);
            i=i+1;
        }   
    }
}

function loading()
{
    $('#main').empty();
    $('#main').append(`
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
}

async function loadDetail(m)
{
    const req = `${detail}${m}?api_key=c4ca21a45bf70168646932fc551d7d47`;
    loading();

    const response = await fetch(req);
    const myJson = await response.json();
    
    
    $('#main').empty();
    $('#main').append(`
    <div class="media">
    <img src="${img}${myJson.backdrop_path}" class="align-self-center mr-3" alt="temp">
        <div class="media-body">
            <h5 style = "font-size: 200%" class="mt-0">${myJson.original_title}</h5>
            <p id = "genre" ><b>Năm phát hành:</b> ${myJson.release_date}
            <br><b>Nhà sản xuất:</b> ${myJson.production_companies[0].name}
            <br><b>Thể loại:</b>
            </p>
            <p id = "actor"><b>Diễn viên:</b> </p>
            <p class="mb-0"><b>Tóm tắt nội dung:</b> ${myJson.overview}</p>

        </div>
    </div>
    `);
   
    loadActor(m);
    loadGenre(myJson);
    loadReview(m);
}

function loadGenre(myJson)
{
    var i=0;
    while (myJson.genres[i])
    {    
        const temp = myJson.genres[i];
        $('#genre').append(`
        <a>${temp.name}, </a>
        `);
        i++;
    }
}

//id la movie_id
async function loadActor(id)
{
    const req = `${detail}${id}/credits?api_key=c4ca21a45bf70168646932fc551d7d47`

    const response = await fetch(req);
    const myJson = await response.json();

    var i=0;
    while (myJson.cast[i])
    {    
        const temp = myJson.cast[i];
        $('#actor').append(`
        <a onclick = "detailActor(${temp.id})"><b style = "color:blue">${temp.name}</b>, </a>
        `);
        i++;
    }

}

async function loadReview(m)
{
    const req = `${detail}${m}/reviews?api_key=c4ca21a45bf70168646932fc551d7d47`

    const response = await fetch(req);
    const myJson = await response.json();

    $('#main').append(`
    <div class="row">
        <div class="col">
            <div id = "review">
                <div id = "review">
                    <h5 class="card-title"><br><br>REVIEW<br></h5>      
                </div>
            </div>
        </div>
    </div>
    
    `);
    
    var i=0;
    while (myJson.results[i])
    {    
        const temp = myJson.results[i];
        $('#review').append(`
            <h6 class="card-subtitle mb-2 text-muted"><br>Người đánh giá: ${temp.author}</h6>
            <p class="card-text">${temp.content}</p>
        `);
        i++;
    }
}

//id : person_id
async function detailActor(id)
{
    const req = `${actordetail}${id}?api_key=c4ca21a45bf70168646932fc551d7d47&language=en-US`

    const response = await fetch(req);
    const myJson = await response.json();

    $('#main').empty();
    $('#main').append(`
    <div class="media">
    <img src="${img}${myJson.profile_path}" class="align-self-center mr-3" alt="temp">
        <div id = "list" class="media-body">
            <h5 style = "font-size: 200%" class="mt-0">${myJson.name}</h5>
            <p class="mb-0"><b>Tiểu sử:</b> ${myJson.biography}</p>
        </div>
    </div>
    `);

    $('#list').append(`
    <h5 style = "font-size: 200%"><br>Các phim của ${myJson.name}</h5>
    <table class="table">
    <thead class="thead-dark">
    <tr>
      <th scope="col">stt</th>
      <th scope="col">Tên phim</th>
      <th scope="col">Ngày phát hành</th>
      <th scope="col">vote average</th>
    </tr>
    </thead>
    <tbody id = "listmovie">

    </tbody>
    </table>
    `)

    fillListMovieActor(myJson.imdb_id);
}

async function fillListMovieActor(id)
{
    const req = `${find}${id}?api_key=c4ca21a45bf70168646932fc551d7d47&language=en-US&external_source=imdb_id`

    const response = await fetch(req);
    const myJson = await response.json();

    var i=0;
    while(myJson.person_results[0].known_for[i])
    {
        const m = myJson.person_results[0].known_for[i];
        $('#listmovie').append(`
        <tr>
            <th scope="row">${i+1}</th>
            <td>${m.title}</td>
            <td>${m.release_date}</td>
            <td>${m.vote_average}</td>
        </tr>
        `);
        i++;
    }
}